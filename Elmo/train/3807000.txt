clogged washer tubes or nozzles the tubing or the nozzles themselves can become clogged with debris
the nozzles which are quite small can easily get clogged by dirt leaves or other debris
frozen washer fluid in extremely cold weather it is possible for washer fluid to freeze
if the fluid is frozen it should thaw as the car warms up
what to expect
a top rated mobile mechanic will come to your home or office to determine the source and cause of the windshield wiper or washer system issue and will then provide a detailed inspection report that includes the scope and cost of the necessary repairs
how important is this service
a malfunctioning windshield wiper or washer system is usually not a life or death situation but it should be addressed as soon as it is convenient
this can become a very real problem if the weather suddenly turns bad as limited visibility can make driving extremely dangerous
wiper gearbox replacement service
what is the wiper gearbox all about
your windshield wipers remove water snow and other impediments allowing you to see the road ahead clearly
windshield wiper systems require linkages and a gearbox to drive them at the preset rate intermittent low high etc
it is generally located at the base of the windshield under the hood and the cowling
the gearbox is directly connected to the wiper arm linkage that causes the windshield wiper pivots to turn and wipe the window
the gears inside the gearbox are subject to wear though the usual failure is due to operation with excess weight on the wiper arms such as snow
this causes the internal gears to bind bend or strip
keep in mind
once a gearbox sees significant wear wipers will not function optimally and eventually stop working altogether
replacing the wiper blades is a good idea anytime a wiper system component needs repair
how it s done
the wiper arms are removed from the wiper gear box
the cowl is removed to access the wiper gear box
the defective wiper gear box is removed and the new one installed
the cowl is reinstalled and the wiper arms are installed onto the gear box
the wipers are tested to work in all positions
our recommendation
the wiper gearbox is a sealed system and cannot be serviced in most instances
when it malfunctions or show signs of failure replacement is necessary
have one of our expert mechanics look at it to ensure a smooth replacement and to professionally inspect wiper arm linkages and other system components
what common symptoms indicate you may need to replace the wiper gearbox
windshield wipers operate intermittently or not at all
wiper speed is inaccurate or inconsistent
how important is this service
because your windshield wipers are responsible for ensuring a clear view of the road during inclement weather it s important to have the wiper gearbox replaced when wear and tear begins to take a toll
we recommend having the problem first diagnosed by one of our expert mechanics who will determine if the issue is the switch the gearbox or the relay and inspect the rest of the wiper system including linkages and wiper arms